# gnome-console

A simple user-friendly terminal emulator for the GNOME desktop

https://gitlab.gnome.org/GNOME/console

<br>

How to clone this repository:
```
git clone https://gitlab.com/rebornos-team/rebornos-packages/gnome-packages/gnome-console.git
```

